package testdata;

import utilities.ReusableMethods;

import java.util.HashMap;
import java.util.Map;

public class TestData {

    public Map<String, Object> expTestData = new HashMap<>();
    String email = "selim.ocakdan";

    public Map<String, Object> postTestData() {
        expTestData.put("name", "Selim Ocakdan");
        expTestData.put("email", ReusableMethods.emailUniqueness(email));
        expTestData.put("gender", "male");
        expTestData.put("status", "active");
        return expTestData;
    }

    public Map<String, Object> postFailTestData() {
        expTestData.put("name", "Pushti Khan");
        expTestData.put("email", "pushti_khan@hessel-vandervort.io");
        expTestData.put("gender", "male");
        expTestData.put("status", "inactive");
        return expTestData;
    }


}

