package utilities;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class ReusableMethods {
    //========Switching Window with Title=====//
    public static void switchToWindow(String targetTitle) {
        String origin = Driver.getDriver().getWindowHandle();
        for (String handle : Driver.getDriver().getWindowHandles()) {
            Driver.getDriver().switchTo().window(handle);
            if (Driver.getDriver().getTitle().equals(targetTitle)) {
                return;
            }
        }
        Driver.getDriver().switchTo().window(origin);
    }

    //========Switching Window with Iterator=====//
    public static void switchToNextWindow() {
        Set<String> allWindows = Driver.getDriver().getWindowHandles();

        Iterator<String> it1 = allWindows.iterator();
        while (it1.hasNext()) {
            String el = it1.next();
            Driver.getDriver().switchTo().window(el);
        }
        //Driver.getDriver().switchTo().window(origin);
    }

    //========Scroll to Webelement========//
    public static void scrollToElement(WebElement element) {
        ((JavascriptExecutor) Driver.getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
    }


    //=======Email Uniqueness=====//
    public static String emailUniqueness(String email) {
        int randomNum = ThreadLocalRandom.current().nextInt(0, 100000);
        String fullEmail = email + randomNum + "@gmail.com";
        return fullEmail;
    }

}