package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import pages.HomePage;
import pages.TechDocsPage;
import utilities.ConfigReader;
import utilities.Driver;
import utilities.ReusableMethods;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class UIStepDefinitions {

    HomePage homePage = new HomePage();
    TechDocsPage techDocsPage = new TechDocsPage();

    @Given("user goes to the main page")
    public void user_goes_to_the_main_page() {
        //reads url from configuration.properties file
        Driver.getDriver().get(ConfigReader.getProperty("url"));
    }

    @When("user clicks on Search icon")
    public void user_clicks_on_Search_icon() {
        homePage.searchIcon.click();
    }

    @When("user searches for {string}")
    public void user_searches_for(String searchKeys) {
        homePage.searchBox.sendKeys(searchKeys);
    }

    @When("user clicks on Search button")
    public void user_clicks_on_Search_button() {
        homePage.searchButton.click();
    }

    @Then("user verifies all the results contain {string} keyword")
    public void user_verifies_all_the_results_contain_keyword(String keyword) {

        //checks all the results contain the searched keyword.
        List<WebElement> keywordList = techDocsPage.keywordListLc;
        for (WebElement w : keywordList) {
            Assert.assertTrue(w.getText().toLowerCase().contains(keyword.toLowerCase()));
        }
    }

    @Given("user goes to the techdocs page")
    public void user_goes_to_the_techdocs_page() {

        //reads url from configuration.properties file
        Driver.getDriver().get(ConfigReader.getProperty("techdocsurl"));
    }

    @When("user clicks on Search dropdown")
    public void user_clicks_on_Search_dropdown() {
        //to avoid no element error, we scroll to the web element.
        ReusableMethods.scrollToElement(techDocsPage.docDropdown);
        techDocsPage.docDropdown.click();
    }

    @When("user searches for {string} on techdocs")
    public void user_searches_for_on_techdocs(String keyword) {

        techDocsPage.searchDropdown.sendKeys(keyword + Keys.ENTER);

    }

    @Then("user verifies the catalogs can be seen for {string}")
    public void userVerifiesTheCatalogsCanBeSeenFor(String keyword) {

        //checks all the results contain the searched file name.
        List<WebElement> resultItems = techDocsPage.resultItemsLc;
        for (WebElement w : resultItems) {
            Assert.assertTrue(w.getText().toUpperCase().contains(keyword.toUpperCase()));
        }
    }


    @When("user clicks on the top catalog")
    public void user_clicks_on_the_top_catalog() throws InterruptedException {
        ReusableMethods.scrollToElement(techDocsPage.documentView);
        techDocsPage.documentView.click();

    }


    @And("user can see the document on a new tab with the name includes {string}")
    public void userCanSeeTheDocumentOnANewTabWithTheNameIncludes(String fileName) {

        //switching to the next window.
        ReusableMethods.switchToNextWindow();
        Assert.assertTrue(Driver.getDriver().getCurrentUrl().contains(fileName));
    }


    @When("user turns back to the first tab")
    public void user_turns_back_to_the_first_tab() {

        //switching to the previous window.
        ReusableMethods.switchToWindow(ConfigReader.getProperty("techdocstitle"));
    }

    @When("user downloads the document")
    public void user_downloads_the_document() {
        ReusableMethods.scrollToElement(techDocsPage.downloadButton);
        techDocsPage.downloadButton.click();
    }

    @Then("verifies the document has been downloaded with file name {string}")
    public void verifiesTheDocumentHasBeenDownloadedWithFileName(String fileName) throws InterruptedException {

        //check if the downloaded document exist in Downloads file.
        String userFolder = System.getProperty("user.home");
        String downloadsPath = "Downloads";
        String path = userFolder + String.format("\\%s\\%s", downloadsPath, fileName);
        Thread.sleep(3000);
        boolean isFileExist = Files.exists(Paths.get(path));
        Assert.assertTrue(isFileExist);

    }
}