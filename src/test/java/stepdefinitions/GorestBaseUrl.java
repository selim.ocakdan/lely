package stepdefinitions;

import io.cucumber.java.en.Given;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class GorestBaseUrl {
    public static RequestSpecification baseUrl;

    @Given("user calls the base url")
    public void user_calls_the_base_url() {
        baseUrl = new RequestSpecBuilder().setBaseUri("https://gorest.co.in/public/").build();

    }
}
