package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;
import testdata.TestData;
import utilities.ConfigReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class WSStepDefinitions {
    Response response;
    TestData reqObj = new TestData();
    Map<String, Object> postReqBody;
    JsonPath json;

    @When("user call the endpoint with {string} for the parameter {string} and {string} for {string}")
    public void user_call_the_endpoint_with_for_the_parameter_and_for(String pathParName1, String pathParam1, String pathParName2, String pathParam2) {
        GorestBaseUrl.baseUrl.pathParams(pathParName1,pathParam1,pathParName2,pathParam2);
    }

    @When("user gets the single user response with path parameters {string} and {string}")
    public void user_gets_the_single_user_response_with_path_parameters_and(String pathParName1, String pathParName2) {
        String endPointPathParams = String.format("{%s}/{%s}",pathParName1,pathParName2 );
        response = given().spec(GorestBaseUrl.baseUrl).when().get(endPointPathParams);
        //response.prettyPrint();

    }

    @Then("verifies the id length")
    public void verifies_the_id_length() {
        json = response.jsonPath();
        List<Integer> actualIds = new ArrayList<>();
        actualIds = (json.getList("data.id"));
        System.out.println(actualIds);

        for (Integer w: actualIds) {
            Assert.assertTrue("id s are not 4 digits",w > 999 && w < 10000);
            //If they are 4 digits it means they are not null but....
            Assert.assertFalse("id s are null", w==0);
            Assert.assertFalse("id s are null", String.valueOf(w).equals(" "));
        }
    }


    @When("user sends single user creation request with path parameters {string} and {string}")
    public void user_sends_single_user_creation_request_with_path_parameters_and(String pathParName1, String pathParName2) {
        String endPointPathParams = String.format("{%s}/{%s}",pathParName1,pathParName2 );
        postReqBody = reqObj.postTestData();
        response = given().
                headers("Authorization","Bearer " + ConfigReader.getProperty("api_bearer_token"),
                        "Content-Type", ContentType.JSON,
                        "Accept",ContentType.JSON).
                spec(GorestBaseUrl.baseUrl).
                body(postReqBody).
                when().
                post(endPointPathParams);
        response.prettyPrint();
    }

    @Then("user verifies the response with expected data")
    public void user_verifies_the_response_with_expected_data() {

        json = response.jsonPath();
        assertEquals(201, response.getStatusCode());
        assertEquals(postReqBody.get("name"),   json.getString("data.name"));
        assertEquals(postReqBody.get("email"),  json.getString("data.email"));
        assertEquals(postReqBody.get("gender"), json.getString("data.gender"));
        assertEquals(postReqBody.get("status"), json.getString("data.status"));

    }

    @Then("user encounters with a fail message")
    public void user_encounters_with_a_fail_message() {
        json = response.jsonPath();
        //System.out.println(json);
        assertEquals(422, response.getStatusCode());
        assertEquals("[email]", json.getString("data.field"));
        assertEquals("[has already been taken]", json.getString("data.message"));

    }

    @And("user sends defined single user creation request with path parameters {string} and {string}")
    public void userSendsDefinedSingleUserCreationRequestWithPathParametersAnd(String pathParName1, String pathParName2) {

        String endPointPathParams = String.format("{%s}/{%s}",pathParName1,pathParName2 );
        postReqBody = reqObj.postFailTestData();
        response = given().
                headers("Authorization","Bearer " + ConfigReader.getProperty("api_bearer_token"),
                        "Content-Type", ContentType.JSON,
                        "Accept",ContentType.JSON).
                spec(GorestBaseUrl.baseUrl).
                body(postReqBody).
                when().
                post(endPointPathParams);
        response.prettyPrint();
    }
}
