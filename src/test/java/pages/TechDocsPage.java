package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.List;

public class TechDocsPage {
    public TechDocsPage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(id = "select2-id_q-container")
    public WebElement docDropdown;

    @FindBy(xpath = "//input[@type='search']")
    public WebElement searchDropdown;

    @FindBy(className = "result-item-title")
    public WebElement resultItems;

    @FindBy(partialLinkText = "View this document")
    public WebElement documentView;

    @FindBy(xpath = "(//a[@class ='button button-secondary icon-pdf'])[1]")
    public WebElement downloadButton;

    @FindBy(className = "result-item-title")
    public List<WebElement> resultItemsLc;

    @FindBy(className = "item-description")
    public List<WebElement> keywordListLc;


}
