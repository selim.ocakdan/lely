package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;


public class HomePage {

    public HomePage() {
        PageFactory.initElements(Driver.getDriver(),this);
    }

    //@FindBy(id = "LELY_SEARCH_TOP")
    @FindBy(xpath = "(//div[@class = 'header-navigation-button__label'])[1]")
    public WebElement searchIcon;

    @FindBy(name = "q")
    public WebElement searchBox;

    @FindBy(xpath= "(//button[@type = 'submit'])[1]")
    public WebElement searchButton;
}
