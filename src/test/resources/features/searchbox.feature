@searchbox @lely
Feature: Search box success
  Scenario: Search box result validation
    Given user goes to the main page
    When user clicks on Search icon
    And user searches for "happy"
    And user clicks on Search button
    Then user verifies all the results contain "happy" keyword
