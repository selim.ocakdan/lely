@getAllUsers @lely
Feature: Get all users
  Background: user calls the base url
    Given user calls the base url
  Scenario: Get all users success
    When user call the endpoint with "version" for the parameter "v1" and "users" for "users"
    And user gets the single user response with path parameters "version" and "users"
    Then verifies the id length
