@postSingleUserSuccessFail @lely
Feature: Post single user success

  Background: user creates the base url
    Given user calls the base url

  @postSingleUserSuccess
  Scenario: user creation success
    When user call the endpoint with "version" for the parameter "v1" and "users" for "users"
    And user sends single user creation request with path parameters "version" and "users"
    Then user verifies the response with expected data
  @postSingleUserFail
  Scenario: user creation fail
    When user call the endpoint with "version" for the parameter "v1" and "users" for "users"
    And user sends defined single user creation request with path parameters "version" and "users"
    Then user encounters with a fail message