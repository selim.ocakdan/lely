@techdocs @lely
Feature: Download the document
  Scenario: Download document validation
    Given user goes to the techdocs page
    When user clicks on Search dropdown
    And user searches for "LUNA EUR" on techdocs
    Then user verifies the catalogs can be seen for "LUNA EUR"
    When user clicks on the top catalog
    And user can see the document on a new tab with the name includes "D-S006VT"
    And user turns back to the first tab
    And user downloads the document
    Then verifies the document has been downloaded with file name "D-S006VT_-.pdf"

