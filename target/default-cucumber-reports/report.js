$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/getgorestusers.feature");
formatter.feature({
  "name": "Get all users",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@getAllUsers"
    },
    {
      "name": "@lely"
    }
  ]
});
formatter.background({
  "name": "user calls the base url",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user calls the base url",
  "keyword": "Given "
});
formatter.match({
  "location": "stepdefinitions.GorestBaseUrl.user_calls_the_base_url()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Get all users success",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@getAllUsers"
    },
    {
      "name": "@lely"
    }
  ]
});
formatter.step({
  "name": "user call the endpoint with \"version\" for the parameter \"v1\" and \"users\" for \"users\"",
  "keyword": "When "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.user_call_the_endpoint_with_for_the_parameter_and_for(java.lang.String,java.lang.String,java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user gets the single user response with path parameters \"version\" and \"users\"",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.user_gets_the_single_user_response_with_path_parameters_and(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verifies the id length",
  "keyword": "Then "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.verifies_the_id_length()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/postgorestusers.feature");
formatter.feature({
  "name": "Post single user success",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@postSingleUserSuccessFail"
    },
    {
      "name": "@lely"
    }
  ]
});
formatter.background({
  "name": "user creates the base url",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user calls the base url",
  "keyword": "Given "
});
formatter.match({
  "location": "stepdefinitions.GorestBaseUrl.user_calls_the_base_url()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user creation success",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@postSingleUserSuccessFail"
    },
    {
      "name": "@lely"
    },
    {
      "name": "@postSingleUserSuccess"
    }
  ]
});
formatter.step({
  "name": "user call the endpoint with \"version\" for the parameter \"v1\" and \"users\" for \"users\"",
  "keyword": "When "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.user_call_the_endpoint_with_for_the_parameter_and_for(java.lang.String,java.lang.String,java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user sends single user creation request with path parameters \"version\" and \"users\"",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.user_sends_single_user_creation_request_with_path_parameters_and(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user verifies the response with expected data",
  "keyword": "Then "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.user_verifies_the_response_with_expected_data()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "user creates the base url",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user calls the base url",
  "keyword": "Given "
});
formatter.match({
  "location": "stepdefinitions.GorestBaseUrl.user_calls_the_base_url()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user creation fail",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@postSingleUserSuccessFail"
    },
    {
      "name": "@lely"
    },
    {
      "name": "@postSingleUserFail"
    }
  ]
});
formatter.step({
  "name": "user call the endpoint with \"version\" for the parameter \"v1\" and \"users\" for \"users\"",
  "keyword": "When "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.user_call_the_endpoint_with_for_the_parameter_and_for(java.lang.String,java.lang.String,java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user sends defined single user creation request with path parameters \"version\" and \"users\"",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.userSendsDefinedSingleUserCreationRequestWithPathParametersAnd(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user encounters with a fail message",
  "keyword": "Then "
});
formatter.match({
  "location": "stepdefinitions.WSStepDefinitions.user_encounters_with_a_fail_message()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/searchbox.feature");
formatter.feature({
  "name": "Search box success",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@searchbox"
    },
    {
      "name": "@lely"
    }
  ]
});
formatter.scenario({
  "name": "Search box result validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@searchbox"
    },
    {
      "name": "@lely"
    }
  ]
});
formatter.step({
  "name": "user goes to the main page",
  "keyword": "Given "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_goes_to_the_main_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks on Search icon",
  "keyword": "When "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_clicks_on_Search_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user searches for \"happy\"",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_searches_for(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks on Search button",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_clicks_on_Search_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user verifies all the results contain \"happy\" keyword",
  "keyword": "Then "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_verifies_all_the_results_contain_keyword(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/techdocs.feature");
formatter.feature({
  "name": "Download the document",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@techdocs"
    },
    {
      "name": "@lely"
    }
  ]
});
formatter.scenario({
  "name": "Download document validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@techdocs"
    },
    {
      "name": "@lely"
    }
  ]
});
formatter.step({
  "name": "user goes to the techdocs page",
  "keyword": "Given "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_goes_to_the_techdocs_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks on Search dropdown",
  "keyword": "When "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_clicks_on_Search_dropdown()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user searches for \"LUNA EUR\" on techdocs",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_searches_for_on_techdocs(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user verifies the catalogs can be seen for \"LUNA EUR\"",
  "keyword": "Then "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.userVerifiesTheCatalogsCanBeSeenFor(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks on the top catalog",
  "keyword": "When "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_clicks_on_the_top_catalog()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user can see the document on a new tab with the name includes \"D-S006VT\"",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.userCanSeeTheDocumentOnANewTabWithTheNameIncludes(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user turns back to the first tab",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_turns_back_to_the_first_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user downloads the document",
  "keyword": "And "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.user_downloads_the_document()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verifies the document has been downloaded with file name \"D-S006VT_-.pdf\"",
  "keyword": "Then "
});
formatter.match({
  "location": "stepdefinitions.UIStepDefinitions.verifiesTheDocumentHasBeenDownloadedWithFileName(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
});